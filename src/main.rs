use clap::{Arg, Command};

fn main() {
    let matches = Command::new("prime-mouse")
        .subcommand_required(true)
        .subcommand(
            Command::new("color")
                .about("Set LED color on mouse")
                .arg_required_else_help(true)
                .arg(Arg::new("hexcode").required(true)),
        )
        .subcommand(
            Command::new("brightness")
                .about("Set LED brightness")
                .arg_required_else_help(true)
                .arg(Arg::new("intensity").required(true)),
        )
        .subcommand(Command::new("save"))
        .get_matches();

    match matches.subcommand() {
        Some(("color", sub_matches)) => {
            let hexcode = sub_matches.value_of("hexcode").expect("required");
            let color = Color::parse(hexcode).expect("malformed hex color (must be rrggbb)");
            match apply_operation_on_device(Operation::LedColor(color)) {
                Err(err) => println!("failed to set color: {}", err),
                _ => (),
            }
        }
        Some(("brightness", sub_matches)) => {
            let intensity = sub_matches.value_of("intensity").expect("required");
            let intensity =
                Intensity::parse(intensity).expect("malformed intensity (positive number 0-255)");
            match apply_operation_on_device(Operation::LedBrightness(intensity)) {
                Err(err) => println!("failed to set brightness color: {}", err),
                _ => (),
            }
        }
        Some(("save", _)) => match apply_operation_on_device(Operation::Save) {
            Err(err) => println!("failed to save settings: {}", err),
            _ => (),
        },
        _ => unreachable!(),
    }
}

#[derive(Debug)]
enum DeviceError {
    Generic(Box<dyn std::error::Error>),
}

impl DeviceError {
    fn from_error<E: 'static + std::error::Error>(err: E) -> Self {
        DeviceError::Generic(Box::new(err))
    }
}

impl From<hidapi::HidError> for DeviceError {
    fn from(err: hidapi::HidError) -> Self {
        DeviceError::from_error(err)
    }
}

impl std::error::Error for DeviceError {}
impl std::fmt::Display for DeviceError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            DeviceError::Generic(err) => write!(f, "Generic failure: {}", err),
        }
    }
}

fn build_set_led_color_payload(color: &Color) -> Vec<u8> {
    let mut payload = Vec::new();
    payload.extend_from_slice(&[0x62, 0x01]);
    payload.extend_from_slice(&[
        color.r, color.g, color.b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, color.r, color.g, color.b, 0xff,
    ]);
    return payload;
}

fn build_set_led_intensity_payload(intensity: &Intensity) -> Vec<u8> {
    let mut payload = Vec::new();
    payload.extend_from_slice(&[0x5f]);
    payload.extend_from_slice(&[intensity.0, 0x00]);
    return payload;
}

fn build_save_payload() -> Vec<u8> {
    [0x59].to_vec()
}

#[derive(Debug, PartialEq, Eq)]
struct Intensity(u8);

impl Intensity {
    fn new(value: u8) -> Self {
        Intensity(value)
    }

    fn parse(value: &str) -> Option<Self> {
        u32::from_str_radix(value, 10)
            .map(|value| value.clamp(0, 255) as u8)
            .map(|value| Intensity::new(value))
            .ok()
    }
}

enum Operation {
    LedColor(Color),
    LedBrightness(Intensity),
    Save,
}

impl Operation {
    fn payload(&self) -> Vec<u8> {
        match self {
            Operation::LedColor(color) => build_set_led_color_payload(color),
            Operation::LedBrightness(intensity) => build_set_led_intensity_payload(intensity),
            Operation::Save => build_save_payload(),
        }
    }
}

fn apply_operation_on_device(operation: Operation) -> Result<(), DeviceError> {
    let hidapi = hidapi::HidApi::new()?;
    let vendor_id = 0x1038;
    let product_id = 0x182e;
    let device = hidapi.open(vendor_id, product_id)?;
    let _ = device.write(&operation.payload())?;
    Ok(())
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct Color {
    r: u8,
    g: u8,
    b: u8,
}

impl Color {
    fn parse(hex: &str) -> Option<Self> {
        if hex.len() != 6 {
            return None;
        }

        let r = u8::from_str_radix(&hex[0..2], 16).ok();
        let g = u8::from_str_radix(&hex[2..4], 16).ok();
        let b = u8::from_str_radix(&hex[4..6], 16).ok();

        match (r, g, b) {
            (Some(r), Some(g), Some(b)) => Some(Color { r, g, b }),
            _ => None,
        }
    }

    fn from_rgb(r: u8, g: u8, b: u8) -> Self {
        Color { r, g, b }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_parse_hex_colors() {
        let cases = [
            (
                Color::parse("aabbcc"),
                Some(Color::from_rgb(0xaa, 0xbb, 0xcc)),
            ),
            (
                Color::parse("010203"),
                Some(Color::from_rgb(0x01, 0x02, 0x03)),
            ),
            (
                Color::parse("ffffff"),
                Some(Color::from_rgb(0xff, 0xff, 0xff)),
            ),
            (Color::parse("faulty"), None),
            (Color::parse(""), None),
            (Color::parse("aabbccdd"), None),
        ];

        cases.iter().for_each(|(input, output)| {
            assert_eq!(input, output);
        });
    }

    #[test]
    fn should_create_using_rgb() {
        assert_eq!(
            Color::from_rgb(12, 77, 99),
            Color {
                r: 12,
                g: 77,
                b: 99
            }
        );
    }

    #[test]
    fn should_parse_intensity_from_string() {
        assert_eq!(Intensity::parse("123"), Some(Intensity::new(123)));
        assert_eq!(Intensity::parse("1777"), Some(Intensity::new(255)));
        assert_eq!(Intensity::parse("not-a-number"), None);
    }
}
